module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
      dev: {
        files: [{
          expand: true,
          cwd: 'less/',
          src: '*.less',
          dest: 'public/styles/',
          ext: '.css'
        }]
      },
      prod: {
        files: {
          'dist/styles.css': ['less/*.less']  
        }
      }
    },
    jshint: {
      files: {
        src: ['public/scripts/*.js']
      }
    },
    watch: {
      less: {
        files: 'less/*.less',
        tasks: ['less:dev'],
      },
      js: {
        files: 'public/scripts/*.js',
        tasks: ['jshint']
      }
    },
    uglify: {
      prod: {
        files: {
          'dist/scripts.min.js': [
            'public/scripts/utils.js', 
            'public/scripts/tManager.js',
            'public/scripts/main.js'
            ]
        }
      }
    },
    cssmin: {
      prod: {
        files:{
          'dist/styles.min.css': 'dist/styles.css'
        }
      }
    },
    processhtml: {
      prod: {
        files:{
          'dist/index.html': 'public/index.html'
        }  
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-processhtml');

  grunt.registerTask('build', ['less:prod','uglify:prod', 'cssmin:prod', 'processhtml:prod']);
};