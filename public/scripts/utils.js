(function(win, doc) {
	'use strict';

	/**
	 * Extending Native Types for most frequently used methods
	 */
	
	if(!String.prototype.supplant){
		String.prototype.supplant = function (o) {
		    return this.replace(/{([^{}]*)}/g,
		        function (a, b) {
		            var r = o[b];
		            return typeof r === 'string' || typeof r === 'number' ? r : a;
		        }
		    );
		};
	}

	/**
	 * DOM and Template Related utils
	 */
	
	var classList = document.documentElement.classList;
	var zTmpltsList = {};

	win.utils = {
		hasClass: function(ele, className) {
			if (classList) {
				return ele && ele.classList.contains(className);
			} else {
				return ele.className.indexOf(" " + className + " ") > -1 ? true : false;
			}
		},
		removeClass: function (ele, className) {
			if (this.hasClass(ele, className)) {
				if (classList) {
					ele.classList.remove(className);
				} else {
					ele.className.replace(className, '');
				}
			}
		},
		addClass: function (ele, className) {
			if (!this.hasClass(ele, className)) {
				if (classList) {
					ele.classList.add(className);
				} else {
					ele.className =  ele.className + " " + className;
				}
			}
		},
		readAllTemplates: function(){
			var zTmplts = doc.getElementById('z-templates').children;
			if(zTmplts) {
				Array.prototype.forEach.call(zTmplts, function(ele, idx, arr){
					var name = ele.getAttribute('name');
					zTmpltsList[name] = {
						name: name,
						tmplt: ele.innerHTML 
					};
				});
			}
		},
		getTemplate: function(name, obj){
			if(zTmpltsList[name]) {
				obj = obj || {};
				return zTmpltsList[name].tmplt.supplant(obj);
			}
			return '';
		},
		getClosestByAttribute: function(el, attribute) {
			/* jshint ignore:start */
			do {
				if (el && el.hasAttribute && el.hasAttribute(attribute)) {
				  return el;
				}
			} while (el = el.parentNode);
			/* jshint ignore:end */
			return null;
		},
		documentReady: function(fn){
			doc.addEventListener("DOMContentLoaded", fn);
		}
	};
	
}(window, document));