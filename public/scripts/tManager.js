(function(win, doc) {
	'use strict';

	/**
	 * [TManager Task List Manager]
	 * @param {[DOM Element]} container element
	 * @param {[Object]} obj
	 */
	function TManager(container, obj){
		this.container = container;
		this.renderContainer(obj);
		this.listcontainer = this.container.querySelector('[data-listcontainer]');
		this.lists = [];
		this._bindEvents();
	}

	TManager.prototype._bindEvents = function() {
		var con = this.container, scope = this;
		con.querySelector('[data-newlist]').addEventListener('click', function(e){
			scope.addList();
		});
	};

	TManager.prototype.renderContainer = function(obj) { 
		this.container.innerHTML = utils.getTemplate('tItemManager', obj);
	};

	TManager.prototype.addList = function(obj) {
		var d = document.createElement('div');
		this.listcontainer.appendChild(d);
		new TList(d);
	};

	window.TManager = TManager;



	/**
	 * [TList Task List]
	 * @param {[DOM Element]} container element
	 * @param {[Object]} obj
	 */
	function TList(container, obj){
		this.container = container;
		this.renderContainer(obj);
		this.itemContainer = this.container.querySelector('[data-itemcontainer]');
		this._bindEvents();
		this.container.querySelector('[data-title]').focus();
	}

	TList.prototype._bindEvents = function() {
		var con = this.container, scope = this;
		con.querySelector('[data-deletelist]').addEventListener('click', function(e){
			scope.destroy();
		});
		con.querySelector('[data-additem]').addEventListener('click', function(e){
			scope.addItem();
		});
		
	};

	TList.prototype.renderContainer = function(obj) {
		this.container.innerHTML = utils.getTemplate('tItemList', obj);
	};

	TList.prototype.addItem = function(obj) {
		var d = document.createElement('div');
		this.itemContainer.appendChild(d);
		new TItem(d);
	};

	TList.prototype.destroy = function(){
		var con = this.container;
		//con.querySelector('[data-deletelist]').removeEventListener('click');
		if(con) {
			con.remove();	
		}
	};

	/**
	 * [TItem Item]
	 * @param {[DOM Element]} container element
	 * @param {[Object]} obj
	 */
	function TItem(container, obj){
		this.container = container;
		this.container.innerHTML = utils.getTemplate('tItem', obj);
		this.textarea = this.container.querySelector('textarea');
		this.showdiv = this.container.querySelector('[data-show]');
		this._bindEvents();
	}

	TItem.prototype._bindEvents = function() {
		var scope = this;
		this.textarea.addEventListener("keyup", function(evt){
			scope.showdiv.innerHTML = evt.target.value;
		});
		this.showdiv.addEventListener("click", function(evt){
			utils.removeClass(scope.textarea, 'hidden');
			utils.addClass(scope.showdiv, 'hidden');
			scope.textarea.focus();
		});
		doc.addEventListener('click',function(evt){
			var c = utils.getClosestByAttribute(evt.target, 'data-item');
			if(!c){
				utils.addClass(scope.textarea, 'hidden');
				utils.removeClass(scope.showdiv, 'hidden');
			}
		});
		this.container.querySelector('[data-deleteitem]').addEventListener('click', function(e){
			scope.destroy();
		});
	};

	TItem.prototype.destroy = function(){
		if(this.container) {
			this.container.remove();	
		}
	};


}(window, document));