prerequisites: NodeJS and NPM

Steps to run - Development Version

1. run 'npm install' in tManager directory to download NPM modules

2. run 'grunt watch' to start less compiling and jshint checking

3. to start server use : 'npm start or 'node index.js'

4. open http://localhost:8000 

Steps to build - Production Version 

1. run 'grunt build' in tManager Directory

2. go to tManager/dist  

3. open index.html